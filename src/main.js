import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'

import '@/plugins/bootstrap'

import '@/plugins/datepicker'

import '@/plugins/vuemoment'

import '@/plugins/axios'

import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

// import moment from 'moment'


Vue.config.performance = true
Vue.use(CoreuiVue, VueMoment, {
  moment,
})


var filter = function(text, length, clamp){
  clamp = clamp || '...';
  var node = document.createElement('div');
  node.innerHTML = text;
  var content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter);

new Vue({
  el: '#app',
  router,
  icons,
  template: '<App/>',
  components: {
    App
  },
}).$mount('#app')


