import Vue from 'vue'
import Axios from 'axios'

Axios.defaults.baseURL = '/ecm_webapp'
Axios.defaults.maxRedirects = 0
Axios.defaults.headers.common.Accept = 'application/json'
Axios.defaults.withCredentials = true

Vue.$http = Axios
Vue.prototype.$http = Axios
