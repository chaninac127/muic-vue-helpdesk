import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

import '@/assets/scss/_custom.scss'

Vue.use(BootstrapVue)
